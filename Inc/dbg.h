
/*
 *  @brief DEBUG: If DEBUG MODE IS ACTIVATED UART1 WILL SEND MESSAGES REPORTING THE STATES
 */
#ifndef DBG_H
#define	DBG_H
#include "pic32_uart.h"

#define DBG_MODE

#ifdef DBG_MODE
#define DBG_MESSAGE(ARG)     UART1_write_string(ARG)
#else
#define DBG_MESSAGE(ARG)     
#endif

#endif	/* DBG_H */

