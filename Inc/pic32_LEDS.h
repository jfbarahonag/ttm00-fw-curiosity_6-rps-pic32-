#include <plib.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <xc.h>
#include "dbg.h"

enum _LED_selection_t{
    LED_1,
    LED_2,
    LED_3,
    LED_R,
    LED_G,
    LED_B
}LED_selection_t;

void LED_toggle(uint8_t LED);
void LED_on(uint8_t LED);
void LED_off(uint8_t LED);
void LED_error_handler(void);
void LED_setup(void);
void LED_all_off(void);
void LED_all_on(void);
