## README

### 1. Project Brief

#### 1.1. Project summary
Rock, paper, scissors (RPS) game is implemented on the code. Mode REFEREE and PLAYER are available.

The game consists of 10 hands or moves as a minimum. Those hands are generated randomly by the players. If 10 hands are reached and tie remains the game must go on until win some player. 

Referee request the hands to the players and send the result to them after two seconds (by default but could be changed adjusting COUNTDOWN_TIME_IN_MS) with next commands:

1* -> Request hand

2* -> Won hand

3* -> Lost hand

4* -> Tied hand

5* -> Won match

6* -> Won match

The referee is allowed to show the winner for each hand LED_1 if player 1 won, LED_2 for player 2 and LED_3 for a tie.

The player has his own LED green (won) and red (lost) sequence after a hand or match. For a hand's results LED RGB is used. For a match's results LED 1 and LED 2 were used.

### 2. Hardware description

#### 2.1. MCU datasheet
- PIC32MX470F512H: http://ww1.microchip.com/downloads/en/devicedoc/60001185g.pdf

- Curiosity Board user guide: http://ww1.microchip.com/downloads/en/DeviceDoc/70005283B.pdf

### 3. Serial commands

#### 3.1. Link to the commands source/header file

#### 3.2. Serial command file for serial terminal
- docklight
	https://docklight.de/downloads/

#### 3.3. Terminal configuration (UART 1, UART 2, UART 3)
- Baud rate = 115200
- Parity = None
- Parity Error Char. --- (ignore)
- Data Bits = 8
- Stop Bits = 1 --Send/Receive Com --- The one assigned by your PC.

### 4. Prerequisites

#### 4.1. SDK version
-  MPLAB V3.61 4.2. IDE:
-  MPLABX IDE V3.61 4.3. Compiler version:
-  XC 32 V1.42 4.4. Project configuration:
-  Categories : Microchip Embedded
-  Projects : Standalone Project
-  Family : All families
-  Device : PIC32MX470F512H
-  Hardware tools : Microchip starter kits
-  Compiler : XC32 (v1.42)
-  Encoding : ISO-8859-1

### 5. Versioning

#### 5.1. Current version of the FW
- V1.0.20191226

#### 5.2. Description of the last version changes
-  Allow to select the mode (REFEREE or PLAYER) to play the game RPS via UART	

### 6. Authors
- Project staff
	Juan Felipe Barahona Gonzalez
- Maintainer contact
	email: felipe.barahona@titoma.com