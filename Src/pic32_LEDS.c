#include "pic32_LEDS.h"

void LED_on(uint8_t LED){
    if (LED==LED_1 || LED==LED_2 || LED==LED_3 || LED==LED_R || LED==LED_G || LED==LED_B){
    switch (LED){
        case LED_1:
            mPORTESetBits(BIT_4);
            break;
        case LED_2:
            mPORTESetBits(BIT_6);
            break;
        case LED_3:
            mPORTESetBits(BIT_7);
            break;
        case LED_R:
            mPORTBClearBits(BIT_10);
            break;
        case LED_G:
            mPORTBClearBits(BIT_3);
            break;
        case LED_B:
            mPORTBClearBits(BIT_2);
            break;
        default:
            break;
        }
    }
    else 
        LED_error_handler();
}

void LED_off(uint8_t LED){
    if (LED==LED_1 || LED==LED_2 || LED==LED_3|| LED==LED_R || LED==LED_G || LED==LED_B){
    switch (LED){
        case LED_1:
            mPORTEClearBits(BIT_4);
            break;
        case LED_2:
            mPORTEClearBits(BIT_6);
            break;
        case LED_3:
            mPORTEClearBits(BIT_7);
            break;
        case LED_R:
            mPORTBSetBits(BIT_10);
            break;
        case LED_G:
            mPORTBSetBits(BIT_3);
            break;
        case LED_B:
            mPORTBSetBits(BIT_2);
            break;
        default:
            break;
        }
    }
    else 
        LED_error_handler();
}

void LED_toggle(uint8_t LED){
    if (LED==LED_1 || LED==LED_2 || LED==LED_3|| LED==LED_R || LED==LED_G || LED==LED_B){
        switch (LED){
        case LED_1:
            mPORTEToggleBits(BIT_4);
            break;
        case LED_2:
            mPORTEToggleBits(BIT_6);
            break;
        case LED_3:
            mPORTEToggleBits(BIT_7);
            break;
        case LED_R:
            mPORTBToggleBits(BIT_10);
            break;
        case LED_G:
            mPORTBToggleBits(BIT_3);
            break;
        case LED_B:
            mPORTBToggleBits(BIT_2);
            break;
        default:
            break;
        }
    }
    else    
        LED_error_handler();  
}

void LED_error_handler(void){
    DBG_MESSAGE("Error LED not valid");
    while(1){}
}

void LED_setup(void) {
    // Initialize LED1, LED2 y LED3
    mPORTESetPinsDigitalOut(BIT_4|BIT_6|BIT_7);
    // Initialize LED RGB
    mPORTBSetPinsDigitalOut(BIT_2|BIT_3|BIT_10);
}

void LED_all_off(void) {
    LED_off(LED_1);
    LED_off(LED_2);
    LED_off(LED_3); 
    LED_off(LED_R);
    LED_off(LED_G);
    LED_off(LED_B); 
}

void LED_all_on(void) {
    LED_on(LED_1);
    LED_on(LED_2);
    LED_on(LED_3); 
    LED_on(LED_R);
    LED_on(LED_G);
    LED_on(LED_B); 
}